# frozen_string_literal: true

require_relative "secure_analyzer_integration_test/version"

module SecureAnalyzerIntegrationTest
  class Error < StandardError; end
  # Your code goes here...
end
