shared_examples "successful scan" do
  it "creates a report" do
    expect(File.exist?(@report_path)).to be_truthy
  end

  describe "exit code" do
    specify{ expect(@exit_code).to eql 0 }
  end
end

shared_examples "failed scan" do
  it "creates no report" do
    expect(File.exist?(@report_path)).to be_falsy
  end

  describe "exit code" do
    specify{ expect(@exit_code).not_to eql 0 }
  end
end
